const {
    EventEmitter
} = require("events")

class CryptoCurrency extends EventEmitter {

    constructor(_name, _price) {
        super();
        this.name = _name;
        this.firstPrice = _price;
        this.price = this.firstPrice

    }

    calPrice(eventName) {
        let result;
        if (this.price === this.firstPrice) {
            return super.emit("noChange", this.name)
        } else {
            result = ((this.price - this.firstPrice) * 100) / this.firstPrice
            return super.emit(eventName, result, this.name)
        }
    }


}



let btc = new CryptoCurrency("BTC", 1200)

btc.on("calculate", (result, name) => {
    console.log(`${name} Changed ${result} %`);
})

btc.on("noChange", (name) => {
    console.log(`The "${name}" Has No Change`);
})




btc.price = 1150

btc.calPrice("calculate")